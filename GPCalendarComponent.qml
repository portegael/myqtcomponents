import QtQuick 2.0


// Custom Calendar Component
Item {
    id: base

    // Returned date
    property var selectedDate : new Date()

    // Background
    property string backgroundColor           : "#2A303E"
    property string selectedDateHighlightColor: "#5A404E"

    // Text
    property string monthTextColor  : "#E5DB82"
    property string weekDayTextColor: "#626A77"
    property string dayTextColor    : "white"
    property string textFamilyFont  : "Ubuntu"
    property int    textPixelSize   : 16

    // Grid
    property string gridLinesColor : backgroundColor
    property bool showGrid         : false

    // Get number of days in selected month
    function monthDays(){
        var d= new Date(selectedDate.getFullYear(), selectedDate.getMonth()+1, 0);
        return d.getDate();
    }

    ListModel {
        id: daysListModel
        ListElement {day: qsTr("Lu")}
        ListElement {day: qsTr("Ma")}
        ListElement {day: qsTr("Me")}
        ListElement {day: qsTr("Je")}
        ListElement {day: qsTr("Ve")}
        ListElement {day: qsTr("Sa")}
        ListElement {day: qsTr("Di")}
    }

    // Background
    Rectangle {
        anchors.fill: parent
        color: base.backgroundColor

        Column {
            anchors.fill: parent

            // Header
            Row {
                id: headerRow
                width: parent.width
                height: parent.height * 0.1


                // Previous Month
                Rectangle {
                    width : parent.width / 5
                    height: parent.height
                    color: base.backgroundColor


                    Image {
                        width: parent.width * 0.5
                        height: parent.height * 0.5
                        anchors.centerIn: parent
                        fillMode: Image.PreserveAspectFit

                        source : "qrc:/left_arrow.png"
                        opacity: previousMonthMA.pressed ? 0.66 : 1
                    }


                    MouseArea {
                        id: previousMonthMA
                        anchors.fill: parent
                        onClicked: {
                            console.log("Previous month clicked")

                            base.selectedDate.setMonth(base.selectedDate.getMonth() - 1)
                            base.selectedDateChanged()
                        }
                    }
                }

                // Selected Month
                Rectangle {
                    width : parent.width * 3 / 5
                    height: parent.height
                    color: base.backgroundColor

                    Text {
                        width: parent.width
                        height: parent.height
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter

                        color: base.monthTextColor
                        font.family: base.textFamilyFont
                        font.bold: true
                        font.capitalization: Font.Capitalize
                        font.pixelSize: base.textPixelSize
                        text: String(Qt.formatDateTime(base.selectedDate, "MMMM")) + " " + base.selectedDate.getFullYear().toString()
                    }
                }

                // Next Month
                Rectangle {
                    width : parent.width / 5
                    height: parent.height
                    color: base.backgroundColor

                    Image {
                        width: parent.width * 0.5
                        height: parent.height * 0.5
                        anchors.centerIn: parent
                        fillMode: Image.PreserveAspectFit

                        source : "qrc:/right_arrow.png"
                        opacity: nextMonthMA.pressed ? 0.66 : 1
                    }

                    MouseArea {
                        id: nextMonthMA
                        anchors.fill: parent
                        onClicked: {
                            console.log("Next month clicked : " + base.selectedDate.getMonth() + base.selectedDate.getFullYear())

                            base.selectedDate.setMonth(base.selectedDate.getMonth() + 1)
                            base.selectedDateChanged()
                        }
                    }
                }
            }

            // Header Day
            Row {
                id: daysRow
                width: parent.width
                height: parent.height * 0.1

                Repeater {
                    width: parent.width
                    height: parent.height
                    clip: true
                    model: daysListModel

                    delegate: Rectangle {
                        color: base.backgroundColor
                        width: parent.width / 7
                        height: parent.height

                        Text {
                            width: parent.width
                            height: parent.height

                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter

                            color: base.weekDayTextColor

                            font.family: base.textFamilyFont
                            font.bold: true
                            font.pixelSize: base.textPixelSize

                            text: String(day)
                        }
                    }
                }
            }

            // Day Grid
            GridView {
                id: dayGrid
                width: parent.width
                height: parent.height * 0.8

                cellWidth: width / 7
                cellHeight: cellWidth

                model : base.monthDays()

                delegate: Rectangle {
                    id: delegateRectangle
                    color: base.backgroundColor
                    width: dayGrid.cellWidth
                    height: dayGrid.cellHeight

                    border.color: base.gridLinesColor
                    border.width: base.showGrid ? 1 : 0

                    // Hiighlight element
                    Rectangle {
                        color: base.selectedDateHighlightColor
                        opacity: 0.35;
                        radius: 50
                        visible: (base.selectedDate.getDate() - 1 === index);
                        anchors.fill: parent;
                    }

                    Text {
                        width: parent.width
                        height: parent.height

                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter

                        color: base.dayTextColor

                        font.family: base.textFamilyFont
                        font.bold: base.selectedDate.getDate() - 1 === index ? true: false
                        font.pixelSize: base.textPixelSize

                        text: index + 1
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            // Change Date if different than current
                            if(base.selectedDate.getDate() - 1 !== index) {
                                base.selectedDate.setDate(index + 1)
                                base.selectedDateChanged()
                            }
                        }
                    }
                }

                highlight: Rectangle {
                    color: "blue"
                }
            }
        }
    }
}
